//$('h1').html('<b>test</b>') // manipulation d'HTML
// $('h1').append('<b>test</b>') // manipulation d'HTML
const maListeDeTaches = [
    "manger",
    "finir le projet",
    "finir Skyrim",
]
/*
for (let i = 0; i < maListeDeTaches.length; i++){
    $('ul').append('<li>'+maListeDeTaches[i]+'</li>')
}
*/
/*
maListeDeTaches.forEach( tache => {
    $('ul').append(`<li>${tache}</li>`);
})*/
const numbers = [
    5,
    7,
    8,
    15,
    3
]
numbers.map(function(nombre){return nombre*2})
maListeDeTaches.map(function(tache) {return '- '+tache})
var phrase = "bonjour tou le monde"
phrase.split(" ").map(function(mot){return "-"+mot+"-"})
numbers.filter(function(number) {return number > 10})

for (i in numbers) {
    // console.log(i)
}
console.log("====");
for(i of numbers) {
    // console.log(i)
}


function maFonction() {
 return "bounjour"
}

var maSecondeFonction = function () {
    return "bonjour2"
}

var maTroisiemeFonction = () => {
    return "bonjour3"
}

var maQuatriemeFonction = (param) => "bonjour4"

var maCiqiemeFonction = nombre => nombre*2

function double(nombre) {
    return nombre*2
}
numbers.map(double)

var user =  {
    direBonjour: function () {
        console.log("bonjour je m'appele "+this.firstName)
    },
    firstName: "william",
    lastName:"Lefebvre",
}
user.lastName
user["firstName"]
var nomDuChamp = "firstName"
user[nomDuChamp]
user.username= "dunarr"
user.direAurevoir = function () {
    console.log("au revoir")
}

function aboyer() {
    return ("waf");
}

var nom = 'toutou'

var chien = {
    nom: nom,
    aboyer: aboyer
}

var chien2 = {
    nom,
    aboyer
}

function addition(premierNombre) {
    return function (secondNombre) {
        return premierNombre + secondNombre
    }
}

ajoute1 = addition(1)
ajoute2 = addition(2)
addition(2)(10)

ajoute1(5)
ajoute2(5)

var chien1 = {nom: "milou", surnom: "toutou"}
var chien2 = {nom: "milou", surnom: "toutou"}
var chien3 = chien1
var chien4 = {...chien1}
// object.assign
chien5 = {...chien1, surnom: "toutou2", adresse: "la cabane au fond du jardin"}

tableau1 = ["a", "b", "c"]
tableau2 = ["d", "e", "f"]
tableau3 = [...tableau1,...tableau2, "g"]

